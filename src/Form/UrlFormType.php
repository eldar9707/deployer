<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Url;

class UrlFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('url', TextType::class, [
                'label'       => 'URL',
                'required'    => true,
                'constraints' => [
                    new Url(),
                ],
                'attr' => [
                    'placeholder' => 'Please, enter URL'
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
