<?php

namespace App\Controller;

use App\Form\UrlFormType;
use App\Service\QrCode\QrCodeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QrCodeController extends AbstractController
{
    private const QR_CODE_IMAGES_PATH = '/app/public/qr-code-images/';

    public function __construct(
        private readonly QrCodeService $qrCodeService,
    ) {}

    #[Route('/', name: 'qrcode_index')]
    public function index(Request $request): Response|RedirectResponse
    {
        $form = $this->createForm(UrlFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $url = $form->get('url')->getData();

            return $this->redirectToRoute('qrcode_show', ['url' => $url]);
        }

        return $this->render('qr_code/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/show', name: 'qrcode_show', methods: 'GET')]
    public function show(Request $request): Response|RedirectResponse
    {
        if ($url = $request->get('url')) {
            $qrCode   = $this->qrCodeService->generateUrlQrCode($url);
            $fileName = \sha1($url) . '.png';
            $path     = self::QR_CODE_IMAGES_PATH . $fileName;

            $this->qrCodeService->saveUrlQrCodeAsFile($url, $path);

            return $this->render('qr_code/show.html.twig', [
                'qrCode'   => $qrCode,
                'fileName' => $fileName,
            ]);
        }

        return $this->redirectToRoute('qrcode_index');
    }
    #[Route('/download', name: 'qrcode_download', methods: 'GET')]
    public function download(Request $request): BinaryFileResponse|RedirectResponse
    {
        $fileName = $request->get('fileName');
        $path     = self::QR_CODE_IMAGES_PATH . $fileName;
        $file     = new \SplFileInfo($path);

        if ($file->isFile()) {
            return $this->file($file);
        }

        return $this->redirectToRoute('qrcode_index');
    }
}
