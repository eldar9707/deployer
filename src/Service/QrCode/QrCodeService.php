<?php

namespace App\Service\QrCode;

interface QrCodeService
{
    public function generateUrlQrCode(string $url): string;

    public function saveUrlQrCodeAsFile(string $url, string $path): void;
}