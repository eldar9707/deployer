<?php

namespace App\Service\QrCode;

use Endroid\QrCode\Builder\BuilderInterface;

class EndroidQrCodeService implements QrCodeService
{
    public function __construct(
        private readonly BuilderInterface $customQrCodeBuilder,
    ) {
    }

    public function generateUrlQrCode(string $url): string
    {
        $this->assertUrl($url);

        $result = $this->customQrCodeBuilder
            ->size(200)
            ->data($url)
            ->build();

        return $result->getDataUri();
    }

    public function saveUrlQrCodeAsFile(string $url, string $path): void
    {
        $this->assertUrl($url);

        $result = $this->customQrCodeBuilder
            ->size(600)
            ->data($url)
            ->build();

        $result->saveToFile($path);
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function assertUrl(string $url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \InvalidArgumentException('Invalid URL: ' . $url);
        }
    }
}