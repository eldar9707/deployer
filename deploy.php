<?php
namespace Deployer;

require 'recipe/symfony.php';
require 'contrib/rsync.php';

// Config

set('keep_releases', 3);
set('rsync_src', __DIR__);
set('rsync_dest','{{release_path}}');

add('rsync', [
    'exclude' => [
        '.idea',
        '.git',
        'deploy.php',
        'vendor',
        'var',
        '.env.local',
    ],
]);

// Hosts

host('deployer_demo')->setRemoteUser('deployer')->setDeployPath('/var/www/deployer-demo');

// Tasks

task('docker:restart', fn() => run('cd {{current_path}} && docker compose down && docker compose up -d --build'));

task('deploy:update_code')->disable();

task('phpunit', fn() => runLocally('bin/phpunit'));

// Hooks

after('deploy:release', 'rsync');
after('deploy:publish', 'docker:restart');
after('deploy:failed', 'deploy:unlock');
before('deploy', 'phpunit');
