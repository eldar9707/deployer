<?php

namespace App\Tests;

use App\Service\QrCode\EndroidQrCodeService;
use Endroid\QrCode\Builder\BuilderInterface;
use Endroid\QrCode\Writer\Result\ResultInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EndroidQrCodeServiceTest extends TestCase
{
    private EndroidQrCodeService $qrCodeService;
    private BuilderInterface|MockObject $mockQrCodeBuilder;
    private ResultInterface|MockObject $mockQrCodeResult;

    public function setUp(): void
    {
        parent::setUp();

        $this->mockQrCodeBuilder = $this->createMock(BuilderInterface::class);
        $this->mockQrCodeResult  = $this->createMock(ResultInterface::class);
        $this->qrCodeService     = new EndroidQrCodeService($this->mockQrCodeBuilder);
    }

    public function testSuccessGenerateQrCode(): void
    {
        $url        = 'https://example.com';
        $qrCodeData = 'data:image/png;base64,QkVHSU46...';

        $this->mockQrCodeBuilder
            ->expects($this->once())
            ->method('size')
            ->with(200)
            ->willReturnSelf();

        $this->mockQrCodeBuilder
            ->expects($this->once())
            ->method('data')
            ->with($url)
            ->willReturnSelf();

        $this->mockQrCodeBuilder
            ->expects($this->once())
            ->method('build')
            ->willReturn($this->mockQrCodeResult);

        $this->mockQrCodeResult
            ->expects($this->once())
            ->method('getDataUri')
            ->willReturn($qrCodeData);

        $result = $this->qrCodeService->generateUrlQrCode($url);

        $this->assertEquals($qrCodeData, $result);
    }

    public function testSuccessSaveUrlQrCodeAsFile(): void
    {
        $url  = 'https://example.com';
        $path = '/path/to/save/qr-code.png';

        $this->mockQrCodeBuilder
            ->expects($this->once())
            ->method('size')
            ->with(600)
            ->willReturnSelf();

        $this->mockQrCodeBuilder
            ->expects($this->once())
            ->method('data')
            ->with($url)
            ->willReturnSelf();

        $this->mockQrCodeBuilder
            ->expects($this->once())
            ->method('build')
            ->willReturn($this->mockQrCodeResult);

        $this->mockQrCodeResult
            ->expects($this->once())
            ->method('saveToFile')
            ->with($path);

        $this->qrCodeService->saveUrlQrCodeAsFile($url, $path);
    }

    public function testFailureGenerateQrCodeWithInvalidUrl(): void
    {
        $url      = 'qwe qwe.com';
        $message  = 'Invalid URL: ' . $url;

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($message);

        $this->qrCodeService->generateUrlQrCode($url);
    }

    public function testFailureSaveUrlQrCodeAsFileWithInvalidUrl(): void
    {
        $url      = 'qwe qwe.com';
        $path     = '/path/to/save/qr-code.png';
        $message  = 'Invalid URL: ' . $url;

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage($message);

        $this->qrCodeService->saveUrlQrCodeAsFile($url, $path);
    }
}
